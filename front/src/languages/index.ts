export const supportLanguages = ['en', 'fr'];


const langNav = navigator.language || supportLanguages[0];
const indO = langNav.indexOf('-');
const finalLang = langNav.substring(0, indO >= 0 ? indO : langNav.length).toLocaleLowerCase();

export const defaultLanguages = supportLanguages.includes(finalLang) ? finalLang : supportLanguages[0];