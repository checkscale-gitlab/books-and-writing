const {model, Schema} = require('mongoose');

const article_schema = new Schema({
    title: [
        {
            lang: String,
            text: String,
        }
    ],
    publishDate: Date,
    lastModified: Date,
    links: [
        {
            type: Schema.Types.ObjectId,
            ref: "Image"
        }
    ],
    contents: [
        {
            lang: String,
            text: String
        }
    ],
    slugs: [
        {
            lang: String,
            text: String,
        }
    ],
    categories: [
        {
            type: Schema.Types.ObjectId,
            ref: "Categorie"
        }
    ]
})

const Article = model("Article", article_schema);

module.exports = Article;
