const {model, Schema} = require('mongoose');

const link_schema = new Schema({
    link: String,
    platform: String,
    image: {
        type: Schema.Types.ObjectId,
        ref: "Image"
    }
})

const Link = model("Link", link_schema);

module.exports = Link;
