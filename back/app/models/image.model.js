const {model, Schema} = require('mongoose');

const image_schema = new Schema({
    link: String,
    type: String,
})

const Image = model("Image", image_schema);

module.exports = Image;
